*** Settings ***
Library    AppiumLibrary

*** Variables ***
${Remote_URL}                http://localhost:4723/wd/hub
${platformName}              Android
${platformVersion}           12.0
${deviceName}                emulator-5554
${appPackage}                com.android.dialer
${appActivity}               com.android.dialer.main.impl.MainActivity
${locator_touchpad}          id=com.android.dialer:id/fab
${locator_btn0}              xpath=//android.widget.FrameLayout[@content-desc="0"]/android.widget.LinearLayout/android.widget.TextView
${locator_btn8}              xpath=//android.widget.FrameLayout[@content-desc="8,TUV"]/android.widget.LinearLayout/android.widget.TextView 
${locator_call}              id=com.android.dialer:id/dialpad_floating_action_button
${locator_endcall}           id=com.android.dialer:id/incall_end_call
${locator_speaker}           id=com.android.dialer:id/incall_third_button
${locator_calllog}           id=com.android.dialer:id/call_log_tab
${locator_detailcall}        xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup[2]/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout
${locator_btnblock}          id=com.android.dialer:id/block_action
${locator_blockmsg}          id=android:id/message
${locator_selectblock}       id=android:id/button1
${locator_unblock}           id=com.android.dialer:id/unblock_action

*** Keywords ***
Set open App
    Open Application    ${Remote_URL}
    ...                 platformName=${platformName}    
    ...                 platformVersion=${platformVersion}    
    ...                 deviceName=${deviceName}  
    ...                 automationName=UiAutomator2    
    ...                 newCommandTimeout=2500 
    ...                 appPackage=${appPackage}    
    ...                 appActivity=${appActivity}   

Click mobile phone
    Wait Until Page Contains    ADD A FAVORITE    3s
    Click Element               ${locator_touchpad}
    Click Element               ${locator_btn0}   
    Click Element               ${locator_btn8}
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 
    Click Element               ${locator_btn8} 

Click button phone
    Wait Until Element Is Visible    ${locator_call}        3s
    Click Element                    ${locator_call}

Wait call finish
    Wait Until Page Contains         Calling                3s
    Wait Until Element Is Visible    ${locator_speaker}    
    Click Element                    ${locator_endcall}
    Wait Until Page Contains         ADD A FAVORITE         3s

Block number
    Wait Until Element Is Visible    ${locator_calllog}     3s
    Click Element                    ${locator_calllog}
    Click Element                    ${locator_detailcall}
    Wait Until Element Is Visible    ${locator_btnblock}    3s
    Click Element                    ${locator_btnblock}
    Wait Until Element Is Visible    ${locator_blockmsg}    3s
    Click Element                    ${locator_selectblock}
    Wait Until Element Is Visible    ${locator_unblock}    3s



*** Test Cases ***
Open phone
    Set open App
    Click mobile phone
    Click button phone
    Wait call finish
    Block number
    Close Application
